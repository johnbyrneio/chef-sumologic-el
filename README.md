sumologic-el
==============
Installs and configures a Sumo Logic collector on RPM-based Linux distributions

Differences from the official SumoLogic cookbook:
-------------------------------------------------
* Installs using RPM installer
* Only supports API key authentication
* Uses data bags to define sources

Requirements
------------
* Internet access to download RPM from 'collectors.sumologic.com' or RPM hosted in a locally accessable repository
* API keys generated from your Sumo Logic account
* Data bag item: sumologic-sources list of sources (See 'Data Bags' section for details)
* Encrypted data bag item: sumologic/credentials (See 'Data Bags' section for details)


Attributes
----------

See attributes/default.rb. It is well commented.

Data Bags
---------

sumologic-el/credentials

```json
{
  "id":"credentials",
  "accessid": ""
  "accesskey": ""
}
```

sumologic-collectors/source

```json
{
  "id":"default",
  "data": {}
}
```

Usage
-----

Just include `sumologic-el` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[sumologic-el]"
  ]
}
```

License and Authors
-------------------
Author: John Byrne
