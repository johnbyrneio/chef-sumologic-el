#
# Cookbook Name:: sumologic-el
# Recipe:: default
#
# Copyright (C) 2014 
#
# Checks to see if Sumo Logic collector is already installed. If not, calls
#   install recipe.

if File.exists? '/opt/SumoCollector'
    Chef::Log.info "Sumo Logic collector already installed."
else
	include_recipe 'sumologic-el::install'
end

service "collector" do
	action [ :enable, :start ]
end

