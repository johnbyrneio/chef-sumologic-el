#
# Cookbook Name:: sumologic-el
# Recipe:: install
#
# Copyright (C) 2014 
#
# Downloads, installs, and configures the Sumo Logic collector
#

Chef::Log.info "Installing Sumo Logic Collector"

sumologic_credentials = Chef::EncryptedDataBagItem.load("sumologic", "credentials")

template "/etc/sumo.conf" do
	variables(:accessid => sumologic_credentials['accessid'],
		      :accesskey => sumologic_credentials['accesskey'])
	action :create
end

sumologic_sources = data_bag_item('sumologic-sources', node['sumologic-el']['sources'])

template "/etc/sumo-sources.json" do
	variables(:sources => sumologic_sources['data'])
	action :create
end

if node['sumologic-el']['web-install']
	remote_file "#{Chef::Config[:file_cache_path]}/SumoCollector.rpm" do
  		source "https://collectors.sumologic.com/rest/download/rpm/64"
  		action :create_if_missing
	end
	package "SumoCollector" do
		source "#{Chef::Config[:file_cache_path]}/SumoCollector.rpm"
		action :install
	end
	file '#{Chef::Config[:file_cache_path]}/SumoCollector.rpm' do
		action :delete
	end
else
	package "SumoCollector" do
		action :install
	end
end

service "collector" do
	action [ :enable, :start ]
end

# Post-Install Clean Up.
file '/etc/sumo.conf' do
  action :delete
end
    
file '/etc/sumo-sources.json' do
  action :delete
end