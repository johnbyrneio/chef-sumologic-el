name             'sumologic-el'
maintainer       'John Byrne'
maintainer_email 'REDACTED FOR NOW'
license          'All rights reserved'
description      'Installs/Configures Sumo Logic Collector on RPM-based Linux distributions'
long_description 'Installs/Configures sumologic-el'
version          '1.0.0'

