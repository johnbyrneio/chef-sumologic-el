# Collector Name: 
# Leave as nil to use the Chef node name
default['sumologic-el']['collector_name'] = nil

# Web installation:
# true: install using lastest version from Sumo Logic website
# false: install using package from local Spacewalk/Satellite/Yum repository
default['sumologic-el']['web-install'] = false

# Collection Sources:
# Requires a data bag called 'sumologic-sources'
# Will look for an item in the 'sumologic-sources' data bag matching 
# the following value that defines the list of sources
default['sumologic-el']['sources'] = 'default'

# Ephemeral Collector:
# true: Will delete collector from Sumo Logic console if inactive for 
#       longer than 12 hours. Collected log data will be retained
# false: Will not delete collector from Sumo Logic console
default['sumologic-el']['ephemeral'] = true

# Clobber Collector's Name:
# true: Will resuse existing collector name
# false: Will not reuse existing collector name
default['sumologic-el']['clobber'] = false